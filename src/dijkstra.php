<?php
  $title = 'Dijkstra Algorithm';
	# jar_type (different value meanings):
	/* 
	runJar: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (no additional dependency JARs)
	runMain: Used if NO Manifest File present in JAR-archive
	runJarWithClasspath: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (additional dependency JARs)
	runApplet: Used specifically for APPLETS
	*/
  $jar_type = 'runJar';
	$jarFile = 'Dijkstra.jar';
  $jarDir = 'dijkstra';
	$sizeW = '700';
  $sizeH = '1000';
  $usageText = "
<h2>Usage</h2>
<b>Sample Network:</b> use button \"example\" (down on the right) to work with a sample network and then use button \"step\" to step
through the execution of the algorithm.
";
  require('template.php');
?>