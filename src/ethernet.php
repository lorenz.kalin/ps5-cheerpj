<?php
  $title = 'Ethernet - Collisions';
	# jar_type (different value meanings):
	/* 
	runJar: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (no additional dependency JARs)
	runMain: Used if NO Manifest File present in JAR-archive
	runJarWithClasspath: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (additional dependency JARs)
	runApplet: Used specifically for APPLETS
	*/
	$jar_type = 'runMain';
	# fqcn: Fully qualified class name i. e. 'ch.educeth.visualdft.VisualDFT';
	$fqcn = 'EthernetCollisions';
  $jarFile = 'EthernetCollisions.jar';
  $jarDir = "ethernet";
  $sizeW = '800';
  $sizeH = '350';
	# $preamble = '<table BORDER="1" bgcolor="#dddddd"><tr><td><img src="ethernet/info.png">';
  $postamble = '<img src="ethernet/info.png">';
  require('template.php');
?>