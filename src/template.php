<html>
<head>
  <title><?php echo($title); ?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
	<script src="https://cjrtnc.leaningtech.com/2.2/loader.js"></script>
  <style>
    body {
      font-family: Roboto, sans-serif;
      background: url("http://rudolf.scheurer.home.hefr.ch/moodle/img/bg.png") no-repeat center center fixed;
      background-size: cover;
    }
    h1, h2, h3 {
      color: #069;
      font-weight: normal;
    }
    div#container {
      margin: 0px auto;
      width: 80%;
      min-width: 800px;
      max-width: 1200px;
      background-color: #fff;
    }
    div#content {
      padding: 10px 10px 0px 20px;
    }
    div#header { }
    div#logo-wrapper {
      padding: 10px 0px 10px 10px;
      background-image: url("http://rudolf.scheurer.home.hefr.ch/moodle/img/cl-logo.svg");
      background-repeat: no-repeat;
      background-size: 200px;
      background-position: 98% 20px;
    }
    div#logo {
      background-image: url("http://rudolf.scheurer.home.hefr.ch/moodle/img/Logo_HEIA-FR_site.jpg");
      width: 544px;
      height: 72px;
    }
    div#bar-blue {
      background-color: #069;
      height: 10px;
    }
    div#bar-gray {
      background-color: #ccc;
      height: 10px;
    }
    div#footer {
      background-color: #373737;
      padding: 5px 10px;
      color: white;
      height: 66px;
      background: #373737 url('http://rudolf.scheurer.home.hefr.ch/moodle/img/logo_hesso.png') no-repeat 98% 10px;
    }
    li { margin-bottom: 6px; }
    code { font-size: 16px; }
    button {
      color: white;
      width: 49%; height: 50px;
      font-family: Roboto, sans-serif;
      font-weight: bold; font-size: 14pt;
      border: none;
    }
    button.webstart { background-color: #4169E1; }
    button.jarfile { background-color: #3CB371; }
    button.webstart:hover { background-color: #5179F1; cursor: pointer; }
    button.jarfile:hover { background-color: #4CC381; cursor: pointer; }
    form { margin-bottom: 0px; }
    .preview-image {
        position: relative;
        width: <?php echo(isset($imageWidth) ? $imageWidth : '500px') ?>;
    }
    .preview-image .preview {
        position: absolute; bottom: 0; left: 0;
        width: <?php echo(isset($imageWidth) ? $imageWidth : '500px') ?>;
        height: <?php echo(isset($imageHeight) ? $imageHeight : '180px') ?>;
        display: none;
        color: #FFF; 
        font-weight: bold; font-size: 20pt;
        text-align: center;
    }
    .preview-image:hover .preview {
        display: block;
        background: rgba(0, 0, 0, .5);
    }
    
  </style>
</head>

<body>
	<div id="container">
		<div id="header">
			<div id="logo-wrapper"><div id="logo"></div></div>
			<div id="bar-blue"></div>
			<div id="bar-gray"></div>
		</div>
		
		<div id="content">
			<p>
			<h1>Animations - <?php echo($title); ?></h1>

			<?php
				if (isset($preamble)) echo ($preamble);
			?>
			<!-- Prepare parent element for HTML5-Canvas -->
			<div id='cheerpyJar' style="padding: 10px;"></div>
			
			<?php
			
			if($jar_type != 'runApplet'){ ?>
				<script>
					cheerpjInit();
					<!-- Create HTML5-Canvas by specifying the dimensions and the parent element -->
					cheerpjCreateDisplay(<?php echo($sizeW) ?>, <?php echo($sizeH) ?>, document.getElementById('cheerpyJar'));
				</script>
			<?php	
			}
			?>
			<?php
			switch($jar_type){
					case "runJar": ?>
							<script>
								<!-- Start the application by specifying the path towards the JAR-file -->
								cheerpjRunJar("/app/include/media/cheerpj/<?php echo($jarDir)?>/<?php echo($jarFile)?>"); 
							</script>
							<p> RunJar </p>
							<?php
							break;
					case "runMain": ?>
							<script>
								<!-- Start the application by specifying the fully qualified class name, the path towards the JAR-file and JAR-dependencies (if any, else leave blank "JAR-Path:") -->
								<!-- (colon separated like "JAR-Path:JAR-dependencies-Path) -->
								cheerpjRunMain("<?php echo($fqcn)?>","/app/include/media/cheerpj/<?php echo($jarDir)?>/<?php echo($jarFile)?>:");
							</script>
							<p> RunMain </p>
							<?php							
							break;
					case "runJarWithClasspath": ?>
							<script>
								<!-- Start the application by specifying the path towards the JAR-file (Classpath), the path towards the JAR-dependencies (if any, else leave blank "JAR-Path","") -->
								cheerpjRunJarWithClasspath("/app/include/media/cheerpj/<?php echo($jarDir)?>/<?php echo($jarFile)?>","");
							</script>
							<p> RunJarWithClasspath </p>
							<?php							
							break;
					case "runApplet": ?>
							<!-- Start the application by specifying archive, code, width and height -->
							<script>cheerpjInit({enablePreciseAppletArchives:true});</script>
							<cheerpj-applet
								archive="<?php echo($appletFile) ?>"
								code="<?php echo($appletClass) ?>"
								width="<?php echo($sizeW) ?>"
								height="<?php echo($sizeH) ?>">
								<p>Could not load Java applet using CheerpJ</p>
							</cheerpj-applet>
							<p> RunApplet </p>
							<?php							
							break;
					default: ?>
							<p> Default </p>
							<?php							
							break;
				}
				
				if (isset($postamble)) echo ($postamble);
			?>
			<p>
		</div> <!-- content -->

		<!-- FOOTER -->
		<div id="footer">
			<br>&copy; <?php echo date("Y") ?> R. Scheurer (HEIA-FR), rudolf.scheurer@hefr.ch
		</div>
		
	</div> <!-- container -->
</body>
</html>