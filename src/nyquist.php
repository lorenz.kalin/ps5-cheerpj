<?php
  $title = 'Nyquist Sampling Limit';
	# jar_type (different value meanings):
	/* 
	runJar: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (no additional dependency JARs)
	runMain: Used if NO Manifest File present in JAR-archive
	runJarWithClasspath: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (additional dependency JARs)
	runApplet: Used specifically for APPLETS
	*/
	$jar_type = 'runApplet';
  $appletFile = 'nyquist/nyquist_limit.jar';
  $appletClass = 'Nyquist';
	$jarFile = 'nyquist_limit.jar';
  $jarDir = 'nyquist';
  $sizeW = '700';
  $sizeH = '594';
  require('template.php');
?>