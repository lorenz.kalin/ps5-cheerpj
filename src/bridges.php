<?php
  $title = 'Transparent Bridges';
	# jar_type (different value meanings):
	/* 
	runJar: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (no additional dependency JARs)
	runMain: Used if NO Manifest File present in JAR-archive
	runJarWithClasspath: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (additional dependency JARs)
	runApplet: Used specifically for APPLETS
	*/
  $jar_type = 'runJar';
	$jarFile = 'TransparentBridges.jar';
  $jarDir = "bridges";
  $sizeW = '600';
  $sizeH = '580';
  require('template.php');
?>