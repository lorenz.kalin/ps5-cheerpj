<?php
  $title = 'Visual DFT';
	# jar_type (different value meanings):
	/* 
	runJar: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (no additional dependency JARs)
	runMain: Used if NO Manifest File present in JAR-archive
	runJarWithClasspath: Used if a Manifest File is present in JAR-archive and Main-Class declared in MANIFEST.mf (additional dependency JARs)
	runApplet: Used specifically for APPLETS
	*/
	$jar_type = 'runJar';
  $jarFile = 'VisualDFT.jar';
  $jarDir = 'dft';
  $sizeW = '874';
  $sizeH = '478';
  require('template.php');
?>